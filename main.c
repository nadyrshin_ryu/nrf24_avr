//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include <delay.h>
#include <nrf24.h>
#include "main.h"


#define nRF_CHANNEL     70
#define nRF_isTX_DEMO   0       // ���� ������ ����-������� � �������� �����������
#define nRF_PaketLen    32      // ���� ������ ������ = 0, �� ������������ ���������� ����� ������

// ����� ���������� � ������ �����������������
uint8_t Addr[] = {0x01, 0x02, 0x03, 0x04, 0x05};
// ����� ������
uint8_t Buff[] = {
  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  0x11, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  0x21, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  0x31, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08
};

uint8_t Pipe = 0;       // ����� ����������
uint8_t Len = 0;        // ����� ������
uint32_t Counter = 0;   // ������� �������
uint32_t TryCounter = 0;// ������� ��������� ������� �������� ������
uint32_t Err = 0;       // ������� ������ ��� ��������
  

void main(void)
{
  // ������������� nRF24
  nrf24_init(nRF_CHANNEL);
  // ��������� ����������, ��������� ������
  nrf24_RxPipe_Setup(0, Addr, nRF_PaketLen);      // ���� ������ ������ = 0, �� ������������ ���������� ����� ������
    
#if (nRF_isTX_DEMO)
  Len = nRF_PaketLen;
  while (1)
  {
    int8_t TryNum = nrf24_Send(Addr, Buff, Len);        // �������� ������
    if (TryNum >= 0)            // ����� ������� ��� ���������
    {
      Counter++;                // ������� ���-�� ������������ �������
      TryCounter += TryNum;     // ������� ����� ���-�� ������� ��������
    }    
    else
      Err++;                    // ������� ���-�� ������ ��� ��������
    
    delay_ms(50);
  }
#else
  while (1)
  {
    Len = nrf24_Recv(&Pipe, Buff);      // ���������, ���� �� �������� ������
    if (Len)    // ������ ����� �� ���������� � Pipe ������ Len
    {
      // ��� ���� ��������� ������

      Counter++;// ������� ���-�� �������� �������
    }
  }
#endif
}
